/*
P_AC version 0.1
����������: pavelost(Pavel_Ostyakov)
������� ��������/��������� ����������: 
					skype: pavel13139
					email: pavelosta@gmail.com
*/

stock AC_MODE_SetPlayerPos(playerid,Float:x,Float:y,Float:z)
	return CallRemoteFunction("AC_SetPlayerPos","ifff",playerid,x,y,z);
#define SetPlayerPos AC_MODE_SetPlayerPos
stock AC_MODE_PutPlayerInVehicle(playerid,vehicleid,seatid)
	return CallRemoteFunction("AC_PutPlayerInVehicle","iii",playerid,vehicleid,seatid);
#define PutPlayerInVehicle AC_MODE_PutPlayerInVehicle
stock AC_MODE_GivePlayerWeapon(playerid,weaponid,ammo)
	return CallRemoteFunction("AC_GivePlayerWeapon","iii",playerid,weaponid,ammo);
#define GivePlayerWeapon AC_MODE_GivePlayerWeapon
stock AC_MODE_ResetPlayerWeapons(playerid)
	return CallRemoteFunction("AC_ResetPlayerWeapons","i",playerid);
#define ResetPlayerWeapons AC_MODE_ResetPlayerWeapons
stock AC_MODE_SetPlayerHealth(playerid,Float:health)
	return CallRemoteFunction("AC_SetPlayerHealth","if",playerid,health);
#define SetPlayerHealth AC_MODE_SetPlayerHealth
stock AC_MODE_SetPlayerArmour(playerid,Float:armour)
	return CallRemoteFunction("AC_SetPlayerArmour","if",playerid,armour);
#define SetPlayerArmour AC_MODE_SetPlayerArmour
stock AC_MODE_AddVehicleComponent(vehicleid,componentid)
	return CallRemoteFunction("AC_AddVehicleComponent","ii",vehicleid,componentid);
#define AddVehicleComponent AC_MODE_AddVehicleComponent
stock AC_MODE_RemoveVehicleComponent(vehicleid,componentid)
	return CallRemoteFunction("AC_RemoveVehicleComponent","ii",vehicleid,componentid);
#define RemoveVehicleComponent AC_MODE_RemoveVehicleComponent
stock AC_MODE_SetVehicleHealth(vehicleid,Float:health)
	return CallRemoteFunction("AC_SetVehicleHealth","if",vehicleid,health);
#define SetVehicleHealth AC_MODE_SetVehicleHealth
stock AC_MODE_RepairVehicle(vehicleid)
	return CallRemoteFunction("AC_RepairVehicle","i",vehicleid);
#define RepairVehicle AC_MODE_RepairVehicle
stock AC_MODE_CreateVehicle(vehicletype, Float:x, Float:y, Float:z, Float:rotation, color1, color2, respawn_delay)
	return CallRemoteFunction("AC_CreateVehicle","iffffiii",vehicletype,x,y,z,rotation,color1,color2,respawn_delay);
#define CreateVehicle AC_MODE_CreateVehicle

stock AC_MODE_AddStaticVehicle(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2)
	return CallRemoteFunction("AC_AddStaticVehicle","iffffii",modelid,spawn_x,spawn_y,spawn_z,z_angle,color1,color2);
#define AddStaticVehicle AC_MODE_AddStaticVehicle
stock AC_MODE_AddStaticVehicleEx(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2,respawn_delay)
	return CallRemoteFunction("AC_AddStaticVehicleEx","iffffiii",modelid,spawn_x,spawn_y,spawn_z,z_angle,color1,color2,respawn_delay);
#define AddStaticVehicleEx AC_MODE_AddStaticVehicleEx
stock AC_MODE_SetVehiclePos(vehicleid,Float:x,Float:y,Float:z)
	return CallRemoteFunction("AC_SetVehiclePos","ifff",vehicleid,x,y,z);
#define SetVehiclePos AC_MODE_SetVehiclePos
stock AC_MODE_SetVehicleZAngle(vehicleid,Float:angle)
	return CallRemoteFunction("AC_SetVehicleZAngle","if",vehicleid,angle);
#define SetVehicleZAngle AC_MODE_SetVehicleZAngle
stock AC_MODE_PlayerSpectatePlayer(playerid,targetplayerid,mode=SPECTATE_MODE_NORMAL)
	return CallRemoteFunction("AC_PlayerSpectatePlayer","iii",playerid,targetplayerid,mode);
#define PlayerSpectatePlayer AC_MODE_PlayerSpectatePlayer
stock AC_MODE_PlayerSpectateVehicle(playerid,targetplayerid,mode=SPECTATE_MODE_NORMAL)
	return CallRemoteFunction("AC_PlayerSpectateVehicle","iii",playerid,targetplayerid,mode);
#define PlayerSpectateVehicle AC_MODE_PlayerSpectateVehicle
stock AC_MODE_RemovePlayerFromVehicle(playerid)
	return CallRemoteFunction("AC_RemovePlayerFromVehicle","i",playerid);
#define RemovePlayerFromVehicle AC_MODE_RemovePlayerFromVehicle
stock AC_MODE_SetPlayerPosFindZ(playerid,Float:x,Float:y,Float:z)
	return CallRemoteFunction("AC_SetPlayerPosFindZ","ifff",playerid,x,y,z);
#define SetPlayerPosFindZ AC_MODE_SetPlayerPosFindZ
forward AC_MODE_OnPlayerConnect(playerid);
#if defined OnPlayerConnect
#undef OnPlayerConnect
#endif
#define OnPlayerConnect AC_MODE_OnPlayerConnect
stock AC_M_TogglePlayerControllable(playerid,toggle)
	return CallRemoteFunction("AC_TogglePlayerControllable","ii",playerid,toggle);
#define TogglePlayerControllable AC_M_TogglePlayerControllable
stock AC_MODE_AddStaticPickup(model, type, Float:X, Float:Y, Float:Z, virtualworld = 0)
    return CallRemoteFunction("AC_AddStaticPickup","iifffi",model,type,X,Y,Z,virtualworld);
#define AddStaticPickup AC_MODE_AddStaticPickup
stock AC_MODE_CreatePickup(model, type, Float:X, Float:Y, Float:Z, virtualworld = 0)
	return CallRemoteFunction("AC_CreatePickup","iifffi",model,type,X,Y,Z,virtualworld );
#define CreatePickup AC_MODE_CreatePickup
stock AC_MODE_DestroyPickup(pickup)
	return CallRemoteFunction("AC_DestroyPickup","i",pickup);
#define DestroyPickup AC_MODE_DestroyPickup
stock AC_MODE_EditObject(playerid, objectid)
	return CallRemoteFunction("AC_EditObject","ii",playerid,objectid);
#define EditObject AC_MODE_EditObject
stock AC_MODE_EditPlayerObject(playerid, objectid)
	return CallRemoteFunction("AC_EditPlayerObject","ii",playerid,objectid);
#define EditPlayerObject AC_MODE_EditPlayerObject
stock AC_MODE_SetPlayerInterior(playerid,interiorid)
	return CallRemoteFunction("AC_SetPlayerInterior","ii",playerid,interiorid);
#define SetPlayerInterior AC_MODE_SetPlayerInterior
stock AC_MODE_SetPlayerVirtualWorld(playerid, worldid)
	return CallRemoteFunction("AC_SetPlayerVirtualWorld","ii",playerid,worldid);
#define SetPlayerVirtualWorld AC_MODE_SetPlayerVirtualWorld
/*new dialogbuttons[2][100];
stock AC_M_ShowPlayerDialog(playerid, dialogid, style, caption[], info[], button1[], button2[])
{
	if(!strlen(button1))
		dialogbuttons[0]=" ";
	else
		strmid(dialogbuttons[0],button1,0,strlen(button1));
	if(!strlen(button2))
		dialogbuttons[1]=" ";
	else
		strmid(dialogbuttons[1],button2,0,strlen(button2));
	return CallRemoteFunction("AC_ShowPlayerDialog","iiissss",playerid,dialogid,style,caption,info,dialogbuttons[0],dialogbuttons[1]);
}
#define ShowPlayerDialog AC_M_ShowPlayerDialog*/
stock AC_MODE_SetVehicleVelocity(vehicleid, Float:x, Float:y, Float:z)
	return CallRemoteFunction("AC_SetVehicleVelocity","ifff",vehicleid,x,y,z);
#define SetVehicleVelocity AC_MODE_SetVehicleVelocity
stock AC_MODE_SetPlayerVelocity(playerid, Float:x, Float:y, Float:z)
	return CallRemoteFunction("AC_SetPlayerVelocity","ifff",x,y,z);
#define SetPlayerVelocity AC_MODE_SetVehicleVelocity
stock AC_MODE_SetPlayerFacingAngle(playerid, Float:ang)
	return CallRemoteFunction("AC_SetPlayerFacingAngle","if",playerid,ang);
#define SetPlayerFacingAngle AC_MODE_SetPlayerFacingAngle
stock AC_MODE_SetPlayerDrunkLevel(playerid,level)
	return CallRemoteFunction("AC_SetPlayerDrunkLevel","ii",playerid,level);
#define SetPlayerDrunkLevel AC_MODE_SetPlayerDrunkLevel
stock AC_MODE_SetPlayerWorldBounds(playerid,Float:x_max,Float:x_min,Float:y_max,Float:y_min)
	return CallRemoteFunction("AC_SetPlayerWorldBounds","iffff",playerid,x_max,x_min,y_max,y_min);
#define SetPlayerWorldBounds AC_MODE_SetPlayerWorldBounds
stock AC_MODE_SetPlayerAmmo(playerid, weaponslot, ammo)
	return CallRemoteFunction("AC_SetPlayerAmmo","iii",playerid,weaponslot,ammo);
#define SetPlayerAmmo AC_MODE_SetPlayerAmmo
stock AC_MODE_SetPlayerArmedWeapon(playerid, weaponid)
	return CallRemoteFunction("AC_SetPlayerArmedWeapon","ii",playerid,weaponid);
#define SetPlayerArmedWeapon AC_MODE_SetPlayerArmedWeapon
stock AC_MODE_SetPlayerWantedLevel(playerid,level)
	return CallRemoteFunction("AC_SetPlayerWantedLevel","ii",playerid,level);
#define SetPlayerWantedLevel AC_MODE_SetPlayerWantedLevel
stock AC_MODE_SetPlayerSpecialAction(playerid,actionid)
	return CallRemoteFunction("AC_SetPlayerSpecialAction","ii",playerid,actionid);
#define SetPlayerSpecialAction AC_MODE_SetPlayerSpecialAction
forward OnPlayerCheat(playerid,code,reason[]);
stock AC_MODE_TogglePlayerSpectating(playerid, toggle)
	return CallRemoteFunction("AC_TogglePlayerSpectating","ii",playerid,toggle);
#define TogglePlayerSpectating AC_MODE_TogglePlayerSpectating
forward AC_BACK_OnPlayerDeath(playerid,killerid,reason);
#define OnPlayerDeath AC_BACK_OnPlayerDeath
