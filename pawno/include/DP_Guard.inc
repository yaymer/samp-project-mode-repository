forward DP_OnPlayerDisconnect(playerid, reason);
forward DP_OnPlayerCheat(playerid, cheat);

public OnPlayerDisconnect(playerid, reason)
{
	if(!reason) Kick(playerid);
	else if(funcidx("DP_OnPlayerDisconnect") != -1) return CallLocalFunction("DP_OnPlayerDisconnect", "ii", playerid, reason);
	return 1;
}

stock DP_AddVehicleComponent(vehicleid, componentid)
{
	CallRemoteFunction("DP_AddVehicleComponent", "dd", vehicleid, componentid);
	return AddVehicleComponent(vehicleid, componentid);
}

stock DP_AddStaticVehicle(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2)
{
	new VehicleID = AddStaticVehicle(modelid, spawn_x, spawn_y, spawn_z, z_angle, color1, color2);
	CallRemoteFunction("DP_AddStaticVehicle", "dffff", VehicleID, spawn_x, spawn_y, spawn_z, z_angle);
	return VehicleID;
}

stock DP_AddStaticVehicleEx(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, color1, color2, respawn_delay)
{
	new VehicleID = AddStaticVehicleEx(modelid, spawn_x, spawn_y, spawn_z, z_angle, color1, color2, respawn_delay);
	CallRemoteFunction("DP_AddStaticVehicleEx", "dffff", VehicleID, spawn_x, spawn_y, spawn_z, z_angle);
	return VehicleID;
}

stock DP_CreateVehicle(vehicletype, Float:x, Float:y, Float:z, Float:rotation, color1, color2, respawn_delay)
{
	new VehicleID = CreateVehicle(vehicletype, x, y, z, rotation, color1, color2, respawn_delay);
	CallRemoteFunction("DP_CreateVehicle", "dffff", VehicleID, x, y, z, rotation);
	return VehicleID;
}

stock DP_TogglePlayerSpectating(playerid, toggle) return CallRemoteFunction("DP_TogglePlayerSpectating", "dd", playerid, toggle);

stock DP_RemoveVehicleComponent(vehicleid, componentid) return CallRemoteFunction("DP_RemoveVehicleComponent", "dd", vehicleid, componentid);
stock DP_SetPlayerHealth(playerid, Float:health) return CallRemoteFunction("DP_SetPlayerHealth", "df", playerid, health);
stock DP_SetPlayerArmour(playerid, Float:armour) return CallRemoteFunction("DP_SetPlayerArmour", "df", playerid, armour);
stock DP_ResetPlayerWeapons(playerid) return CallRemoteFunction("DP_ResetPlayerWeapons", "d", playerid);
stock DP_GivePlayerWeapon(playerid, weaponid, ammo) return CallRemoteFunction("DP_GivePlayerWeapon", "ddd", playerid, weaponid, ammo);
stock DP_SetVehicleToRespawn(vehicleid) return CallRemoteFunction("DP_SetVehicleToRespawn", "d", vehicleid);
stock DP_SetPlayerPos(playerid, Float:x, Float:y, Float:z) return CallRemoteFunction("DP_SetPlayerPos", "dfff", playerid, x, y, z);
stock DP_SetVehiclePos(vehicleid, Float:x, Float:y, Float:z) return CallRemoteFunction("DP_SetVehiclePos", "dfff", vehicleid, x, y, z);
stock DP_SetSpawnInfo(playerid, team, skin, Float:x, Float:y, Float:z, Float:Angle, weapon1, weapon1_ammo, weapon2, weapon2_ammo, weapon3, weapon3_ammo) return CallRemoteFunction("DP_SetSpawnInfo", "dddffffdddddd", playerid, team, skin, x, y, z, Angle, weapon1, weapon1_ammo, weapon2, weapon2_ammo, weapon3, weapon3_ammo);
stock DP_AddPlayerClass(modelid, Float:spawn_x, Float:spawn_y, Float:spawn_z, Float:z_angle, weapon1, weapon1_ammo, weapon2, weapon2_ammo, weapon3, weapon3_ammo) return CallRemoteFunction("DP_AddPlayerClass", "dffffdddddd", modelid, spawn_x, spawn_y, spawn_z, z_angle, weapon1, weapon1_ammo, weapon2, weapon2_ammo, weapon3, weapon3_ammo);
stock DP_PutPlayerInVehicle(playerid, vehicleid, seatid) return CallRemoteFunction("DP_PutPlayerInVehicle", "ddd", playerid, vehicleid, seatid);	
stock DP_SetPlayerInterior(playerid, interior) return CallRemoteFunction("DP_SetPlayerInterior", "dd", playerid, interior);
stock DP_SetPlayerVirtualWorld(playerid, zworld) return CallRemoteFunction("DP_SetPlayerVirtualWorld", "dd", playerid, zworld);

#define SetPlayerInterior DP_SetPlayerInterior
#define SetPlayerVirtualWorld DP_SetPlayerVirtualWorld
#define SetPlayerHealth DP_SetPlayerHealth
#define SetPlayerArmour DP_SetPlayerArmour
#define AddVehicleComponent DP_AddVehicleComponent
#define AddStaticVehicle DP_AddStaticVehicle
#define AddStaticVehicleEx DP_AddStaticVehicleEx
#define CreateVehicle DP_CreateVehicle
#define RemoveVehicleComponent DP_RemoveVehicleComponent
#define ResetPlayerWeapons DP_ResetPlayerWeapons
#define GivePlayerWeapon DP_GivePlayerWeapon
#define SetVehicleToRespawn DP_SetVehicleToRespawn
#define SetPlayerPos DP_SetPlayerPos
#define SetVehiclePos DP_SetVehiclePos
#define SetSpawnInfo DP_SetSpawnInfo
#define TogglePlayerSpectating DP_TogglePlayerSpectating
#define PutPlayerInVehicle DP_PutPlayerInVehicle
#define AddPlayerClass DP_AddPlayerClass

#if defined _ALS_OnPlayerDisconnect
    #undef OnPlayerDisconnect
#else
    #define _ALS_OnPlayerDisconnect
#endif
#define OnPlayerDisconnect DP_OnPlayerDisconnect
